module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: theme => ({
      ...theme('colors'),
      'bluelogin': '#428b91',
      'blueloginbutton': '#00637B',
      'colorcard1': '#5cc3c0',
      'colorcard2': '#8a3035',
      'colorcardactivate1': '#eb9806',
      'colorbottom1': '#0f6b82',
      'colorbottom2': '#b7e0de',
    }),
    extend: {
      backgroundImage: theme => ({
        'fondo-login': "url('../public/img/login/minaLogin.jpg')",
        'login-pattern':
          "linear-gradient(to top, rgba(67, 124, 144, 0.9), rgba(190, 227, 219, 0.7))",
        'fondo-main': "url('../public/fondo.png')",
      }),
      spacing: {
        '': '',
      }
    },
  },
  variants: {
    extend: {
      zIndex: ['hover', 'active'],
    },
  },
  plugins: [],
}
