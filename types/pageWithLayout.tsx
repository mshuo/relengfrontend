import {NextPage} from 'next';
import AdminLayout from '../layouts/adminLayout';

type PageWithAdminLayoutType = NextPage & {layout: typeof AdminLayout}

type PageWithLayoutType = PageWithAdminLayoutType;

export default PageWithLayoutType;
