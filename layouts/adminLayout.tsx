import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  PhoneIcon,
  PlayIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,
  XIcon,
  UserCircleIcon,
} from '@heroicons/react/outline';
import { ChevronDownIcon } from '@heroicons/react/solid';

const options = [
  {
    name: 'Niveles',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "#",
    href: '#',
    icon: null,
  },
  {
    name: 'Añadir Nivel',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/admin/AddNivel",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Ver Niveles',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Documentos',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: null,
  },
  {
    name: 'Subir Bloque',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Subir individual',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Interactivos',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: null,
  },
  {
    name: 'Ver Interactivos',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Añadir nuevo',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Añadir Bloque',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Videos',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: null,
  },
  {
    name: 'Ver Videos',
    description: 'Get a better understanding of where your traffic is coming from.',
    route: "/",
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Añadir nuevo',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: CursorClickIcon,
  },
  {
    name: 'Añadir bloque',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: CursorClickIcon,
  },
  {
    name: 'Paseos Virtuales',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: null,
  },
  {
    name: 'Ver Paseos Virtuales',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: CursorClickIcon,
  },
  {
    name: 'Añadir nuevo',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: CursorClickIcon,
  },
  {
    name: 'Añadir bloque',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: CursorClickIcon,
  },
  {
    name: 'Evaluaciones',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/",
    href: '#',
    icon: null,
  },
  {
    name: 'Preguntas',
    description: 'Speak directly to your customers in a more meaningful way.',
    route: "/admin/Preguntas",
    href: '#',
    icon: CursorClickIcon,
  },
  { name: 'Examenes', 
    description: "Your customers' data will be safe and secure.", 
    route: "/admin/Examenes",
    href: '#', 
    icon: ShieldCheckIcon },
  {
    name: 'Resultados',
    description: "Connect with third-party tools that you're already using.",
    route: "/",
    href: '#',
    icon: ViewGridIcon,
  },
  {
    name: 'Capsulas',
    description: 'Build strategic funnels that will drive your customers to convert',
    route: "/",
    href: '#',
    icon: RefreshIcon,
  },
  {
    name: 'Usuarios',
    description: 'Procesos de Evaluacion',
    route: "/",
    href: '#',
    icon: RefreshIcon,
  },
]
const solutions = [
  {
    name: 'Mi Perfil',
    description: 'Get a better understanding of where your traffic is coming from.',
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Historial',
    description: 'Get a better understanding of where your traffic is coming from.',
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: 'Configuracion',
    description: 'Speak directly to your customers in a more meaningful way.',
    href: '#',
    icon: CursorClickIcon,
  },
  { name: 'Seguridad', description: "Your customers' data will be safe and secure.", href: '#', icon: ShieldCheckIcon },
  {
    name: 'Integracion',
    description: "Connect with third-party tools that you're already using.",
    href: '#',
    icon: ViewGridIcon,
  },
  {
    name: 'Automatizacion',
    description: 'Build strategic funnels that will drive your customers to convert',
    href: '#',
    icon: RefreshIcon,
  },
]
const callsToAction = [
  { name: 'Watch Demo', href: '#', icon: PlayIcon },
  { name: 'Contact Sales', href: '#', icon: PhoneIcon },
]
const resources = [
  {
    name: 'Help Center',
    description: 'Get all of your questions answered in our forums or contact support.',
    href: '#',
    icon: SupportIcon,
  },
  {
    name: 'Guides',
    description: 'Learn how to maximize our platform to get the most out of it.',
    href: '#',
    icon: BookmarkAltIcon,
  },
  {
    name: 'Events',
    description: 'See what meet-ups and other events we might be planning near you.',
    href: '#',
    icon: CalendarIcon,
  },
  { name: 'Security', description: 'Understand how we take your privacy seriously.', href: '#', icon: ShieldCheckIcon },
]
const recentPosts = [
  { id: 1, name: 'Boost your conversion rate', href: '#' },
  { id: 2, name: 'How to use search engine optimization to drive traffic to your site', href: '#' },
  { id: 3, name: 'Improve your customer experience', href: '#' },
]

interface LayoutProps{
  children: React.ReactNode
}

const Layout:React.FunctionComponent<LayoutProps> = ({children}) => {
  return (
    <>
      <Head>
        <title>Admin</title>
        <meta name="description" content="Admin" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="flex flex-col h-screen">
          <div className="relative bg-white">

            <div className="max-w-full px-4 mx-auto sm:px-6">
              <div className="py-6 border-b-2 border-gray-100 ">
                <Popover.Group as="nav" className="flex justify-between">
                  <Popover className="flex">
                    {({open})=>(
                      <>
                        <div className="flex space-x-4">
                          <Popover.Button className="flex inline-flex items-center justify-center p-2 text-gray-400 bg-white md:hidden rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <span className="sr-only">Open menu</span>
                            <MenuIcon className="w-6 h-6" aria-hidden="true" />
                          </Popover.Button>
                          <a href="#">
                            <span className="sr-only">Workflow</span>
                            <img
                              className="w-auto h-8 sm:h-10"
                              src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                              alt=""
                            />
                          </a>
                        </ div>
                        <Transition
                          show={open}
                          as={Fragment}
                          enter="duration-200 ease-out"
                          enterFrom="opacity-0 scale-95"
                          enterTo="opacity-100 scale-100"
                          leave="duration-100 ease-in"
                          leaveFrom="opacity-100 scale-100"
                          leaveTo="opacity-0 scale-95"
                        >
                          <Popover.Panel
                            focus
                            static
                            className="absolute top-0 left-0 z-10 w-full max-w-xs pl-0 transition transform origin-top-left"
                          >
                            <div className="bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-y-2 divide-gray-50">
                              <div className="px-5 pt-5 pb-6">
                                <div className="flex items-center justify-between">
                                  <div className="-mr-2">
                                    <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                      <span className="sr-only">Close menu</span>
                                      <XIcon className="w-6 h-6" aria-hidden="true" />
                                    </Popover.Button>
                                  </div>
                                  <div>
                                    <img
                                      className="w-auto h-8"
                                      src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                                      alt="Workflow"
                                    />
                                  </div>
                                </div>
                                <div className="mt-6">
                                  <nav className="grid gap-y-8">
                                    {options.map((item) => (
                                      <a
                                        key={item.name}
                                        href={item.href}
                                        className="flex items-center p-3 -m-3 rounded-md hover:bg-gray-50"
                                      >
                                        {item.icon && <item.icon className="flex-shrink-0 w-6 h-6 text-indigo-600" aria-hidden="true" />}
                                        {item.icon!==null && <span className="ml-3 text-base font-medium text-gray-900">{item.name}</span>}
                                        {item.icon === null && <span className="w-full ml-3 text-xl font-medium text-indigo-500 border-b-2">{item.name}</span>}
                                      </a>
                                    ))}
                                  </nav>
                                </div>
                              </div>
                            </div>
                          </Popover.Panel>
                        </Transition>
                      </>
                    )}
                  </Popover>
                  <Popover className="flex">
                    {({open})=>(
                      <>
                        <div className="flex space-x-4">
                          <Popover.Button className="flex inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <span className="sr-only">Open menu</span>
                            <UserCircleIcon className="w-6 h-6" aria-hidden="true" />
                          </Popover.Button>
                        </ div>
                        <Transition
                          show={open}
                          as={Fragment}
                          enter="duration-200 ease-out"
                          enterFrom="opacity-0 scale-95"
                          enterTo="opacity-100 scale-100"
                          leave="duration-100 ease-in"
                          leaveFrom="opacity-100 scale-100"
                          leaveTo="opacity-0 scale-95"
                        >
                          <Popover.Panel
                            focus
                            static
                            className="absolute top-0 right-0 z-10 w-auto max-w-xs p-2 transition transform origin-top-right"
                          >
                            <div className="bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-y-2 divide-gray-50">
                              <div className="px-5 pt-5 pb-6">
                                <div className="flex items-center justify-between">
                                  <div>
                                    <img
                                      className="w-auto h-8"
                                      src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                                      alt="Workflow"
                                    />
                                  </div>
                                  <div className="-mr-2">
                                    <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                      <span className="sr-only">Close menu</span>
                                      <XIcon className="w-6 h-6" aria-hidden="true" />
                                    </Popover.Button>
                                  </div>
                                </div>
                                <div className="mt-6">
                                  <nav className="grid gap-y-8">
                                    {solutions.map((item) => (
                                      <a
                                        key={item.name}
                                        href={item.href}
                                        className="flex items-center p-3 -m-3 rounded-md hover:bg-gray-50"
                                      >
                                        <item.icon className="flex-shrink-0 w-6 h-6 text-indigo-600" aria-hidden="true" />
                                        <span className="ml-3 text-base font-medium text-gray-900">{item.name}</span>
                                      </a>
                                    ))}
                                  </nav>
                                </div>
                              </div>
                              <div className="px-5 py-6 space-y-6">
                                <div>
                                  <a
                                    href="#"
                                    className="flex items-center justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700"
                                  >
                                    Cerrar Sesion
                                  </a>
                                </div>
                              </div>
                            </div>
                          </Popover.Panel>
                        </Transition>
                      </>
                    )}
                  </Popover>
                </Popover.Group>
              </div>
            </div>
          </div>
          <div className="relative flex flex-1 bg-gray-200">
            <div className="hidden w-64 h-full p-6 overflow-y-auto bg-white md:block shadow-sm space-y-6">
              <nav className="h-0 grid gap-y-8">
                {options.map((item) => (
                  <Link 
                    href={item.route}
                  >
                    <a
                      key={item.name}
                      href={item.href}
                      className="flex items-center p-3 -m-3 rounded-md hover:bg-gray-50"
                    >
                      {item.icon && <item.icon className="flex-shrink-0 w-6 h-6 text-indigo-600" aria-hidden="true" />}
                      {item.icon!==null && <span className="ml-3 text-base font-medium text-gray-900">{item.name}</span>}
                      {item.icon === null && <span className="w-full ml-3 text-xl font-medium text-indigo-500 border-b-2">{item.name}</span>}
                    </a>
                  </Link>
                ))}
              </nav>
            </div>
            <div className="flex-1 p-6 bg-gray-100">
              {children}
            </div>
          </div>
        </div>
      </main>
    </>
  )
}


export default Layout;
