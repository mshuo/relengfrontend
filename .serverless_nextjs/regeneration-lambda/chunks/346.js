exports.id = 346;
exports.ids = [346];
exports.modules = {

/***/ 32341:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(85893);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(41664);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9008);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(67294);
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9325);
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(95699);








const options = [{
  name: 'Niveles',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "#",
  href: '#',
  icon: null
}, {
  name: 'Añadir Nivel',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/admin/AddNivel",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Ver Niveles',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Documentos',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: null
}, {
  name: 'Subir Bloque',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Subir individual',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Interactivos',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: null
}, {
  name: 'Ver Interactivos',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Añadir nuevo',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Añadir Bloque',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Videos',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: null
}, {
  name: 'Ver Videos',
  description: 'Get a better understanding of where your traffic is coming from.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Añadir nuevo',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Añadir bloque',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Paseos Virtuales',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: null
}, {
  name: 'Ver Paseos Virtuales',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Añadir nuevo',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Añadir bloque',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Evaluaciones',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/",
  href: '#',
  icon: null
}, {
  name: 'Preguntas',
  description: 'Speak directly to your customers in a more meaningful way.',
  route: "/admin/Preguntas",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Examenes',
  description: "Your customers' data will be safe and secure.",
  route: "/admin/Examenes",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ShieldCheckIcon */ .FjK
}, {
  name: 'Resultados',
  description: "Connect with third-party tools that you're already using.",
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ViewGridIcon */ .XOb
}, {
  name: 'Capsulas',
  description: 'Build strategic funnels that will drive your customers to convert',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .RefreshIcon */ .DuK
}, {
  name: 'Usuarios',
  description: 'Procesos de Evaluacion',
  route: "/",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .RefreshIcon */ .DuK
}];
const solutions = [{
  name: 'Mi Perfil',
  description: 'Get a better understanding of where your traffic is coming from.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Historial',
  description: 'Get a better understanding of where your traffic is coming from.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ChartBarIcon */ .DEq
}, {
  name: 'Configuracion',
  description: 'Speak directly to your customers in a more meaningful way.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CursorClickIcon */ .UNN
}, {
  name: 'Seguridad',
  description: "Your customers' data will be safe and secure.",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ShieldCheckIcon */ .FjK
}, {
  name: 'Integracion',
  description: "Connect with third-party tools that you're already using.",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ViewGridIcon */ .XOb
}, {
  name: 'Automatizacion',
  description: 'Build strategic funnels that will drive your customers to convert',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .RefreshIcon */ .DuK
}];
const callsToAction = [{
  name: 'Watch Demo',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .PlayIcon */ .o1U
}, {
  name: 'Contact Sales',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .PhoneIcon */ .qWc
}];
const resources = [{
  name: 'Help Center',
  description: 'Get all of your questions answered in our forums or contact support.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .SupportIcon */ .CE5
}, {
  name: 'Guides',
  description: 'Learn how to maximize our platform to get the most out of it.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .BookmarkAltIcon */ .owZ
}, {
  name: 'Events',
  description: 'See what meet-ups and other events we might be planning near you.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .CalendarIcon */ .Que
}, {
  name: 'Security',
  description: 'Understand how we take your privacy seriously.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .ShieldCheckIcon */ .FjK
}];
const recentPosts = [{
  id: 1,
  name: 'Boost your conversion rate',
  href: '#'
}, {
  id: 2,
  name: 'How to use search engine optimization to drive traffic to your site',
  href: '#'
}, {
  id: 3,
  name: 'Improve your customer experience',
  href: '#'
}];

const Layout = ({
  children
}) => {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(next_head__WEBPACK_IMPORTED_MODULE_2__.default, {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
        children: "Admin"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
        name: "description",
        content: "Admin"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
        rel: "icon",
        href: "/favicon.ico"
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("main", {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-col h-screen",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "relative bg-white",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "max-w-full px-4 mx-auto sm:px-6",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "py-6 border-b-2 border-gray-100 ",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Group, {
                as: "nav",
                className: "flex justify-between",
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover, {
                  className: "flex",
                  children: ({
                    open
                  }) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                      className: "flex space-x-4",
                      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Button, {
                        className: "flex inline-flex items-center justify-center p-2 text-gray-400 bg-white md:hidden rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500",
                        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                          className: "sr-only",
                          children: "Open menu"
                        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .MenuIcon */ .Oqj, {
                          className: "w-6 h-6",
                          "aria-hidden": "true"
                        })]
                      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                        href: "#",
                        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                          className: "sr-only",
                          children: "Workflow"
                        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                          className: "w-auto h-8 sm:h-10",
                          src: "https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg",
                          alt: ""
                        })]
                      })]
                    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Transition, {
                      show: open,
                      as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                      enter: "duration-200 ease-out",
                      enterFrom: "opacity-0 scale-95",
                      enterTo: "opacity-100 scale-100",
                      leave: "duration-100 ease-in",
                      leaveFrom: "opacity-100 scale-100",
                      leaveTo: "opacity-0 scale-95",
                      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Panel, {
                        focus: true,
                        static: true,
                        className: "absolute top-0 left-0 z-10 w-full max-w-xs pl-0 transition transform origin-top-left",
                        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                          className: "bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-y-2 divide-gray-50",
                          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "px-5 pt-5 pb-6",
                            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                              className: "flex items-center justify-between",
                              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "-mr-2",
                                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Button, {
                                  className: "inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500",
                                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "sr-only",
                                    children: "Close menu"
                                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .XIcon */ .b0D, {
                                    className: "w-6 h-6",
                                    "aria-hidden": "true"
                                  })]
                                })
                              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                  className: "w-auto h-8",
                                  src: "https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg",
                                  alt: "Workflow"
                                })
                              })]
                            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                              className: "mt-6",
                              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
                                className: "grid gap-y-8",
                                children: options.map(item => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                  href: item.href,
                                  className: "flex items-center p-3 -m-3 rounded-md hover:bg-gray-50",
                                  children: [item.icon && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(item.icon, {
                                    className: "flex-shrink-0 w-6 h-6 text-indigo-600",
                                    "aria-hidden": "true"
                                  }), item.icon !== null && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "ml-3 text-base font-medium text-gray-900",
                                    children: item.name
                                  }), item.icon === null && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "w-full ml-3 text-xl font-medium text-indigo-500 border-b-2",
                                    children: item.name
                                  })]
                                }, item.name))
                              })
                            })]
                          })
                        })
                      })
                    })]
                  })
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover, {
                  className: "flex",
                  children: ({
                    open
                  }) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                      className: "flex space-x-4",
                      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Button, {
                        className: "flex inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500",
                        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                          className: "sr-only",
                          children: "Open menu"
                        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .UserCircleIcon */ .lMe, {
                          className: "w-6 h-6",
                          "aria-hidden": "true"
                        })]
                      })
                    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Transition, {
                      show: open,
                      as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                      enter: "duration-200 ease-out",
                      enterFrom: "opacity-0 scale-95",
                      enterTo: "opacity-100 scale-100",
                      leave: "duration-100 ease-in",
                      leaveFrom: "opacity-100 scale-100",
                      leaveTo: "opacity-0 scale-95",
                      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Panel, {
                        focus: true,
                        static: true,
                        className: "absolute top-0 right-0 z-10 w-auto max-w-xs p-2 transition transform origin-top-right",
                        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                          className: "bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-y-2 divide-gray-50",
                          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "px-5 pt-5 pb-6",
                            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                              className: "flex items-center justify-between",
                              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                  className: "w-auto h-8",
                                  src: "https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg",
                                  alt: "Workflow"
                                })
                              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "-mr-2",
                                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Popover.Button, {
                                  className: "inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500",
                                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "sr-only",
                                    children: "Close menu"
                                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_5__/* .XIcon */ .b0D, {
                                    className: "w-6 h-6",
                                    "aria-hidden": "true"
                                  })]
                                })
                              })]
                            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                              className: "mt-6",
                              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
                                className: "grid gap-y-8",
                                children: solutions.map(item => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                  href: item.href,
                                  className: "flex items-center p-3 -m-3 rounded-md hover:bg-gray-50",
                                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(item.icon, {
                                    className: "flex-shrink-0 w-6 h-6 text-indigo-600",
                                    "aria-hidden": "true"
                                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "ml-3 text-base font-medium text-gray-900",
                                    children: item.name
                                  })]
                                }, item.name))
                              })
                            })]
                          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "px-5 py-6 space-y-6",
                            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                href: "#",
                                className: "flex items-center justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700",
                                children: "Cerrar Sesion"
                              })
                            })
                          })]
                        })
                      })
                    })]
                  })
                })]
              })
            })
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "relative flex flex-1 bg-gray-200",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "hidden w-64 h-full p-6 overflow-y-auto bg-white md:block shadow-sm space-y-6",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
              className: "h-0 grid gap-y-8",
              children: options.map(item => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                href: item.route,
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                  href: item.href,
                  className: "flex items-center p-3 -m-3 rounded-md hover:bg-gray-50",
                  children: [item.icon && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(item.icon, {
                    className: "flex-shrink-0 w-6 h-6 text-indigo-600",
                    "aria-hidden": "true"
                  }), item.icon !== null && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: "ml-3 text-base font-medium text-gray-900",
                    children: item.name
                  }), item.icon === null && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: "w-full ml-3 text-xl font-medium text-indigo-500 border-b-2",
                    children: item.name
                  })]
                }, item.name)
              }))
            })
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "flex-1 p-6 bg-gray-100",
            children: children
          })]
        })]
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Layout);

/***/ }),

/***/ 14453:
/***/ (function() {

/* (ignored) */

/***/ })

};
;