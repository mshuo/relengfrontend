import React from 'react';
import Head from 'next/head';
import Image from 'next/image';
import {useRouter} from 'next/router';
import {ChevronLeftIcon, ChevronRightIcon, PhoneIcon, LocationMarkerIcon, UserIcon, LockClosedIcon, ChevronDownIcon, LightningBoltIcon} from '@heroicons/react/solid';

import PageWithLayoutType from '../../types/pageWithLayout';

import MainLayout from '../../layouts/mainLayout';

const Main:React.FunctionComponent = () => {
  const Router = useRouter();
  const changePagePrevious = () => {
    Router.back();
  };
  const changePage = () => {
    Router.push('/detalles');
  };
  const changePageVideo = () => {
    Router.push('/video');
  };
  const changePageGeneral = () => {
    Router.push('/especifico');
  };

  return (
    <div>
      <Head>
        <title>Main</title>
        <meta name="description" content="Login de la aplicacion" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="static">
          <div className="absolute inset-0 flex w-screen h-screen transition-all ease-in-out duration-1000 transform translate-x-0 slide">
            <Image src="/img/login/minaLogin.jpg" alt="Logo" layout="fill" objectFit="cover" className="" />
          </div>
          <div className="absolute inset-0 flex w-screen h-screen transition-all ease-in-out duration-1000 transform translate-x-full slide">
            <Image src="/img/login/minaLogin.jpg" alt="Logo" layout="fill" objectFit="cover" className="" />
          </div>
          <div className="absolute inset-0 flex w-screen h-screen transition-all ease-in-out duration-1000 transform translate-x-full slide">
            <Image src="/img/login/minaLogin.jpg" alt="Logo" layout="fill" objectFit="cover" className="" />
          </div>
          <div className="absolute inset-0 flex w-screen h-screen transition-all ease-in-out duration-1000 transform translate-x-full slide">
            <Image src="/img/login/minaLogin.jpg" alt="Logo" layout="fill" objectFit="cover" className="" />
          </div>
        </div>
        {/* body */}
        <div className="absolute flex flex-col-reverse order-1 w-auto min-w-full lg:flex-row lg:min-h-screen">
          {/* Primera Columna  */}
          <div className="flex flex-col justify-end flex-1 min-h-full bg-opacity-10">
            {/* Header Menu */}
            <div className="flex items-center justify-between hidden h-20 bg-white shadow-2xl lg:flex">
              <div className="relative h-12 mx-3 my-2 ml-8 w-44">
                <Image src="/img/logo.png" alt="Logo" layout="fill" objectFit="cover" />
              </div>
              <div className="flex items-center justify-end h-full mr-20 font-semibold tracking-tighter text-blue-900">
                <div className="flex items-center justify-start h-full px-6 hover:bg-colorbottom1 hover:bg-opacity-10" onClick={changePageGeneral}>  
                  <p>
                    EPC1
                  </p>
                  <ChevronDownIcon className="w-5 "/>
                </div>  
                <div className="flex items-center justify-start h-full px-6 hover:bg-colorbottom1 hover:bg-opacity-10" onClick={changePageGeneral}>  
                  <p>
                    EPC2
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start h-full px-6 hover:bg-colorbottom1 hover:bg-opacity-10" onClick={changePageGeneral}>  
                  <p>
                    EPC2 SAID
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start h-full px-6 hover:bg-colorbottom1 hover:bg-opacity-10" onClick={changePageGeneral}>  
                  <p>
                    EPC2 RAR
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
              </div>
            </div>
            {/* Seccion Intemedia */}
            <section className="container flex flex-1 h-full px-8 py-8 ml-auto bg-white bg-opacity-80">
              <div className="flex flex-col flex-grow h-full">
                <div className="w-full h-auto">
                  <p className="px-3 py-2 text-base font-semibold text-yellow-600">Menu Interactivo</p>
                  <p className="px-3 py-1 font-sans text-4xl font-bold text-green-600">Menu Principal</p>
                </div>
                <div className="flex-1 w-full h-full lg:h-0 ">
                  <div className="flex flex-wrap justify-center h-full px-4 pt-12 overflow-y-auto border border-gray-200 rounded-lg shadow-xl">
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                    <button className="flex flex-col mx-px my-4 rounded-lg shadow-2xl shadow-inner z-90 bg-colorcard1 hover:bg-colorcardactivate1 transform hover:scale-105 w-44 h-60 duration-500 hover:z-100"
                      onClick={changePage}
                    >
                      <div className="flex items-center justify-center flex-1 w-full h-full">
                        <ChevronDownIcon className="w-32 h-32 text-white"/>
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 w-full h-full">
                        <div className="flex items-center justify-center h-1/5 ">
                          <p className="text-xl font-semibold text-center text-white">
                            Titulo
                          </p>
                        </div>
                        <div className="flex items-center justify-center px-4 py-2 h-4/5">
                          <p className="items-center text-xs text-center text-white align-middle">
                            lorem iosum fd fsd ffsad fs dafdsa fdsaf dsaf 
                          </p>
                        </div>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
            </section>
            {/* Seccion Final */}
            <div className="flex flex-col px-6 py-5 text-white shadow-2xl md:justify-center bg-colorbottom1 md:justify-between md:flex-row space-y-2">
              {/*
              <div className="flex flex-col w-auto bg-red-300 justify-items-center md:justify-start md:flex-row space-x-14 space-y-2">
              */}
              <div className="flex flex-col justify-center md:justify-start md:items-center md:flex-row space-y-2 space-x-1">
                <div className="relative w-56 mx-auto h-14">
                  <Image src="/img/logo.png" alt="Logo" layout="fill" objectFit="cover" />
                </div>
                <div className="flex flex-col justify-center px-2 ">
                  <p className="flex flex-row items-center justify-center text-xs font-semibold text-white md:justify-start md:mx-3">Informacion</p>
                  <div className="flex flex-row items-center justify-center md:justify-start">
                    <LocationMarkerIcon className="w-5 h-5 mx-3 "/>
                    <p className="text-xs text-justify">Av. Apoquindo 4001, Piso 18. Los Condes, Santiago, Chile </p>
                  </div>
                  <div className="flex flex-row items-center justify-center md:justify-start">
                    <PhoneIcon className="w-5 h-5 mx-3 "/>
                    <p className="text-xs text-justify">(56-2) 2798 7000</p>
                  </div>
                </div>
            {/* 
                <div className="flex flex-col flex-1 w-full ml-0 bg-green-300 justify- md:w-52">
                  <p className="text-xs font-semibold text-center text-white md:text-left">Informacion</p>
                  <div className="flex flex-row w-full">
                    <LocationMarkerIcon className="w-8 h-full m-1 mt-0"/>
                    <p className="text-xs text-justify">Av. Apoquindo 4001, Piso 18. Los Condes, Santiago, Chile </p>
                  </div>
                  <div className="flex flex-row ">
                    <PhoneIcon className="w-4 h-full m-1 mt-0 "/>
                    <p className="text-xs ">(56-2) 2798 7000</p>
                  </div>
                </div>
             */}
              </div>
              <div className="flex items-center justify-center sm:justify-end">
                <div className="flex items-center">
                  <LightningBoltIcon className="h-5"/>
                  <p className="text-sm font-semibold">Cerrar Sesion</p>
                </div>
              </div>
            </div>
          </div>
          {/* Segunda Columna */}
          <div className="flex flex-col justify-between flex-1">
            <div className="flex flex-row justify-end min-w-full">
              <div>
                <div className="flex flex-col h-auto max-w-full py-2 pl-5 text-white bg-gray-500 md:py-5 pr-14 space-y-2 md:space-y-6 bg-opacity-50">
                  <p className="flex justify-end text-xl font-bold md:text-5xl ">
                    PROYECTO INCO.
                  </p>
                  <p className="flex justify-end text-lg font-normal md:text-4xl ">
                    Infraestuctura Complementaria.
                  </p>
                </div>
              </div>
            </div>
            <div className="flex flex-row items-center justify-end flex-1 min-w-full py-2">
              <div className="flex flex-col w-2/3 text-white bg-colorcard2 md:w-2/6 p-7 bg-opacity-60 space-y-6">
                <p className="pb-4 text-xl font-semibold border-b-4 md:text-4xl">EPC2</p>
                <p className="flex-1 text-lg">Lorfsdffjsadlkfj klfdsj flkjsdflkjd lkfjdskl jlkd jfkldsj flkdjlk dsfjlk jfdlkj lkdfjl kdjlkf jdlskfj ljslkj</p>
                <div className="flex justify-center">
                  <button className="px-6 py-2 text-base text-white border-4 border-solid border-colorcard2 md:text-lg rounded-3xl">
                    Planta Concentradora
                  </button>
                </div>
              </div>
            </div>
            <div className="flex flex-col justify-end min-w-full ">
              <div className="flex flex-row items-center justify-between px-20 py-8">
                <button className="flex items-center justify-start bg-yellow-500 rounded-full h-14 w-14 bg-opacity-60">
                  <ChevronLeftIcon className="w-full h-full text-white"/>
                </button>
                <div className="space-x-4">
                  <button className="w-4 h-4 border-2 border-yellow-500 rounded-full bg-yellow-0">
                  </button>
                  <button className="w-4 h-4 bg-yellow-500 border-2 border-yellow-500 rounded-full">
                  </button>
                  <button className="w-4 h-4 border-2 border-yellow-500 rounded-full bg-yellow-0">
                  </button>
                  <button className="w-4 h-4 border-2 border-yellow-500 rounded-full bg-yellow-0">
                  </button>
                </div>
                <button className="flex items-center justify-center bg-yellow-500 rounded-full h-14 w-14 bg-opacity-60">
                  <ChevronRightIcon className="w-full h-full text-white" />
                </button>
              </div>
            </div>
          </div>
          <div className="flex items-center justify-between h-20 bg-white shadow-2xl lg:hidden">
              <div className="h-12 mx-3 my-2 ml-8 bg-gray-200 w-44">
                Icon
              </div>
              <div className="flex items-center justify-end mr-20 font-semibold tracking-tighter text-blue-900 space-x-12">
                <div className="flex items-center justify-start">  
                  <p>
                    EPC1
                  </p>
                  <ChevronDownIcon className="w-5 "/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2 SAID
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2 RAR
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
              </div>
            </div>
        </div>
      </main>
    </div>
  )
}

(Main as PageWithLayoutType).layout = MainLayout;

export default Main;
