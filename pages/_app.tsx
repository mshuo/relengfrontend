import React, {ReactNode} from 'react';
import '../styles/globals.css'
import PageWithLayoutType  from '../types/pageWithLayout';

interface AppLayoutProps{
  Component: PageWithLayoutType,
  pageProps: any
  children: ReactNode
}

const MyApp:React.FunctionComponent<AppLayoutProps> = ({ Component, pageProps, children }) => {

  const Layout = Component.layout || children;

  return <Layout><Component {...pageProps} /></Layout>
  
}

export default MyApp;
