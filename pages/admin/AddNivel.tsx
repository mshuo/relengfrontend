import React from 'react';
import Image from 'next/image';
// layout for page
import adminLayout from '../../layouts/adminLayout';

function AddNivel() {
  return (
    <>
      <div className="flex flex-col">
        <p>
          INGRESO DE NUEVO NIVEL DE JERARQUIA
        </p>
        <div className="">
          <input className="w-1/3 px-3 py-3 placeholder-gray-200 bg-black rounded-xl" type="text" placeholder="Intoduzca el nombre del nivel..." />
        </div>
        <p>
          Banners
        </p>
        <div className="flex flex-row items-center">
          <p className="mr-6">
            Seleccione las imagenes (ancho x altura)
          </p>
          <button className="px-4 py-3 bg-blue-700 rounded-xl">
            <p className="text-white">
              Añadir Imagenes
            </p>
          </button>
        </div>
        <div className="h-96 bg-origin-content bg-fondo-login" >
          
          q
        </div>
        <p>
          Ubicación
        </p>
        <div className="flex flex-row-reverse items-center">
          <button className="px-4 py-3 bg-gray-700 rounded-xl">
            <p className="text-white">
              Volver
            </p>
          </button>
          <button className="px-4 py-3 bg-blue-700 rounded-xl">
            <p className="text-white">
              Añadir Nivel
            </p>
          </button>
        </div>
      </div>
    </>
  );
}

AddNivel.layout = adminLayout;

export default AddNivel;
