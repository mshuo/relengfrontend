import React, {useState, useEffect} from 'react';
// layout for page
import adminLayout from '../../layouts/adminLayout';
import {FolderIcon} from '@heroicons/react/outline';

import {InferGetStaticPropsType} from 'next';

type Post = {
  data: any
}


export const getStaticProps = async () =>{
  const res = await fetch('http://relengapiv1.relengcorp.com/api/question/getAll');
  const posts:any =await res.json();
  return {
    props:{
      posts: posts.data,
    }
  }
}

function Examenes({posts}:InferGetStaticPropsType <typeof getStaticProps>) {
  const [resultado,setResultado] = useState<Array<string>>([]);
  useEffect(() => {
    setResultado(posts);
  }, [posts]);
  console.log(resultado);
  return (
    <>
      <div className="container w-full h-full ">
        <div className="container flex items-center justify-center w-full h-full p-5">
          <div className="flex flex-col items-start justify-start w-full h-full p-8 mx-auto bg-white space-y-8 rounded-md">
            <p className="w-full text-2xl text-gray-600 border-b-2 border-gray-500">
              Lista de Examenes
            </p>
            <button className="px-4 py-2 font-medium text-white bg-indigo-600 rounded-md">
              Nuevo
            </button>
            <div className="w-full space-y-8">
              <p className="text-lg text-gray-600">Home</p>
              <div className="overflow-x-auto space-y-4">
                <table className="items-center max-w-full min-w-full bg-transparent border-collapse">
                  <thead>
                    <tr>
                      <th className="px-6 py-3 text-base font-semibold text-left text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Nombre
                      </th>
                      <th className="px-6 py-3 text-base font-semibold text-left text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Inicio
                      </th>
                      <th className="px-6 py-3 text-base font-semibold text-left text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Fin
                      </th>
                      <th className="px-6 py-3 text-base font-semibold text-left text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Tipo
                      </th>
                      <th className="px-6 py-3 text-base font-semibold text-center text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Estado
                      </th>
                      <th className="px-6 py-3 text-base font-semibold text-center text-gray-500 uppercase align-middle border-b-4 whitespace-nowrap bg-gray-50">
                        Acciones
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th className="px-6 py-2 text-sm font-normal text-left text-black align-middle bg-white border-b-2">
                        lorem fsd f 
                      </th>
                      <th className="px-6 py-2 text-sm font-normal text-left text-black align-middle bg-white border-b-2">
                        Inicio
                      </th>
                      <th className="px-6 py-2 text-sm font-normal text-left text-black align-middle bg-white border-b-2">
                        Fin
                      </th>
                      <th className="px-6 py-2 text-sm font-normal text-left text-black align-middle bg-white border-b-2">
                        Tipo
                      </th>
                      <th className="px-6 py-2 text-sm font-normal text-left text-black align-middle bg-white border-b-2">
                        <div className="flex flex-row items-center justify-center space-x-4">
                          <p>
                            Activado
                          </p>
                          <button className="px-3 py-2 border-2 border-gray-300 rounded-md">
                            Desactivar
                          </button>
                        </div>
                      </th>
                      <th className="px-6 py-2 text-sm font-normal text-center text-black align-middle bg-white border-b-2">
                        <div className="flex flex-row items-center justify-center text-sm space-x-3">
                          <p className="text-indigo-500">
                            Mostrar
                          </p>
                          <p className="text-indigo-500">
                            Editar
                          </p>
                          <p className="text-red-500">
                            Eliminar
                          </p>
                        </div>
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Examenes.layout = adminLayout;

export default Examenes;
