import React from 'react';
// layout for page
import adminLayout from '../../layouts/adminLayout';
import {FolderIcon} from '@heroicons/react/outline';

function Preguntas() {
  return (
    <>
      <div className="container w-full h-full ">
        <div className="container flex items-center justify-center w-full h-full p-5">
          <div className="flex flex-col items-start justify-start w-full h-full p-8 mx-auto bg-white space-y-8 rounded-md">
            <p className="w-full text-2xl text-gray-600 border-b-2 border-gray-500">
              Preguntas
            </p>
            <button className="px-4 py-2 font-medium text-white bg-indigo-600 rounded-md">
              Nuevo
            </button>
            <div className="w-full space-y-8">
              <p className="text-lg text-gray-600">Home</p>
              <div className="space-y-4">
                <div className="flex flex-row items-center justify-between w-full px-4 py-6 bg-gray-200 rounded-md">
                  <div className="flex flex-row items-center space-x-3">
                    <div className="w-auto">
                      <FolderIcon className="w-10 h-10"/> 
                    </div>
                    <p>
                      Concentradora
                    </p>
                  </div>
                  <div className="flex flex-row items-center text-sm space-x-3">
                    <p className="text-indigo-500">
                      Editar
                    </p>
                    <p className="text-red-500">
                      Eliminar
                    </p>
                  </div>
                </div>
                <div className="flex flex-row items-center justify-between w-full px-4 py-6 bg-gray-200 rounded-md">
                  <div className="flex flex-row items-center space-x-3">
                    <div className="w-auto">
                      <FolderIcon className="w-10 h-10"/> 
                    </div>
                    <p>
                      Concentradora
                    </p>
                  </div>
                  <div className="flex flex-row items-center text-sm space-x-3">
                    <p className="text-indigo-500">
                      Editar
                    </p>
                    <p className="text-red-500">
                      Eliminar
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Preguntas.layout = adminLayout;

export default Preguntas;
