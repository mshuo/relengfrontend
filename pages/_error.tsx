import { NextPageContext } from "next";
import Error, { ErrorProps as NextErrorProps } from "next/error";
import React from "react";

class MyError extends React.Component<NextErrorProps> {
  public static getInitialProps({ res, err }: NextPageContext) {
    const statusCode =
      (res && res.statusCode) || (err && err.statusCode) || null;

    return { statusCode };
  }

  public render() {
    return <Error statusCode={this.props.statusCode} />;
  }
}

export default MyError;
