import React from 'react';
import Head from 'next/head';
import Image from 'next/image';
import {useRouter} from 'next/router';
import {ChevronLeftIcon, ChevronRightIcon, PhoneIcon, LocationMarkerIcon, UserIcon, LockClosedIcon, ChevronDownIcon, LightningBoltIcon} from '@heroicons/react/solid';

import PageWithLayoutType from '../../types/pageWithLayout';

import MainLayout from '../../layouts/mainLayout';

const Detail:React.FunctionComponent = () => {
  const Router = useRouter();
  const changePagePrevious = () => {
    Router.back();
  };
  return (
    <div>
      <Head>
        <title>Main</title>
        <meta name="description" content="Login de la aplicacion" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="bg-center bg-no-repeat bg-cover bg-fondo-login">
        {/* body */}
        <div className="flex flex-col justify-end flex-1 min-h-screen bg-white bg-opacity-80">
          {/* Header Menu */}
          <div className="flex items-center justify-between hidden h-20 bg-white shadow-2xl lg:flex">
            <div className="flex justify-between w-1/2 mx-3 my-2 ml-8 ">
              <div className="relative h-12 w-44">
                <Image src="/img/logo.png" alt="Logo" layout="fill" objectFit="cover" />
              </div>
              <div className="flex items-center justify-end mr-20 font-semibold tracking-tighter text-blue-900 space-x-12">
                <div className="flex items-center justify-start">  
                  <p>
                    EPC1
                  </p>
                  <ChevronDownIcon className="w-5 "/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2 SAID
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
                <div className="flex items-center justify-start">  
                  <p>
                    EPC2 RAR
                  </p>
                  <ChevronDownIcon className="w-5 h-5"/>
                </div>  
              </div>
            </div>
            <div className="flex items-center justify-end mr-20 font-semibold tracking-tighter text-yellow-400 ">
              <div className="flex items-center justify-start">  
                <p>
                  Menu Principal
                </p>
              </div>  
            </div>
          </div>
          {/* Seccion Intemedia */}
          <section className="container flex-1 h-0 px-8 py-8 mx-auto bg-white bg-opacity-80">
            <div className="flex flex-col flex-grow max-h-full min-h-full space-y-3">
              <div className="flex ">
                <button className="w-auto px-3 border-2 border-white rounded-lg" onClick={changePagePrevious}>
                  <div className="flex items-center">
                    <ChevronLeftIcon className="w-auto h-6 m-0 -ml-2 text-blue-700"/>
                    <p className="text-blue-700">Atras</p>
                  </div>
                </button>
              </div>
              <div className="flex flex-grow w-full h-auto p-3 rounded-lg bg-opacity-40 space-y-2">
                <div className="flex flex-col w-5/12 min-h-full mt-28 space-y-3">
                  <p className="px-3 text-xl font-medium text-green-500">Paseo Virtual</p>
                  <p className="px-3 text-3xl font-medium text-yellow-400">Proceso de Chancado Primario y Transporte de Mineral Grueso</p>
                  <p className="px-3 text-sm font-normal text-black">jfkjsdl ljf lkdfj lskfj adlskfj lkds lksjdf lkjñk lsdjfñl kijñofjrlkf jofkjkdfi adikmñlkm colfjdlks jfrkdsfkjd ioasfjl dksjfñosakldfjñlsdfl</p>
                </div>
                <div className="flex items-center justify-center w-full min-h-full border-2 shadow-xl rounded-xl">
                  <p className="px-3 text-base font-semibold text-green-500">Video</p>
                </div>
              </div>
            </div>
          </section>
          {/* Seccion Final */}
          <div className="flex flex-col justify-center px-6 py-5 text-white shadow-2xl bg-colorbottom1 md:justify-between md:flex-row space-y-2">
            <div className="flex flex-col justify-center md:justify-start md:items-center md:flex-row space-y-2 space-x-1 md:space-x-10">
              <div className="flex flex-col justify-center w-56 space-y-4">
                <div className="relative w-56 mx-auto h-14">
                  <Image src="/img/logo.png" alt="Logo" layout="fill" objectFit="cover" />
                </div>
                <p className="w-56 text-xs text-justify">
                  jflksjf lsfjlkfj lfjlds kfjlkfjsldkfj sadlkfj lsdkfj dslfkjsd lkfj alskdj 
                </p>
              </div>
              <div className="flex flex-col justify-center px-2 ">
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <p className="text-xs font-bold text-justify">Areas</p>
                </div>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <p className="text-xs text-justify">EPC1</p>
                </div>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <p className="text-xs text-justify">EPC1</p>
                </div>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <p className="text-xs text-justify">EPC1</p>
                </div>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <p className="text-xs text-justify">EPC1</p>
                </div>
              </div>
              <div className="flex flex-col justify-center px-2 ">
                <p className="flex flex-row items-center justify-center text-xs font-semibold text-white md:justify-start md:mx-3">Informacion</p>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <LocationMarkerIcon className="w-5 h-5 mx-3 "/>
                  <p className="text-xs text-justify">Av. Apoquindo 4001, Piso 18. Los Condes, Santiago, Chile </p>
                </div>
                <div className="flex flex-row items-center justify-center md:justify-start">
                  <PhoneIcon className="w-5 h-5 mx-3 "/>
                  <p className="text-xs text-justify">(56-2) 2798 7000</p>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-center sm:justify-end">
              <div className="flex items-center">
                <LightningBoltIcon className="h-5"/>
                <p className="text-sm font-semibold">Cerrar Sesion</p>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

(Detail as PageWithLayoutType).layout = MainLayout;

export default Detail;
