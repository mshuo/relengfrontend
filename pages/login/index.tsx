import React, {useState} from 'react';
import Head from 'next/head';
import Image from 'next/image';
import {useRouter} from 'next/router';
import {UserIcon, LockClosedIcon} from '@heroicons/react/solid';

import PageWithLayoutType from '../../types/pageWithLayout';

import MainLayout from '../../layouts/mainLayout';

type State = {
  text: string,
};

const Login:React.FunctionComponent = () => {

  const useFormInput = (initialValue:any) => {
    const [value, setValue] = useState<any>('');

    const handleChange = (e: React.FormEvent<HTMLInputElement>):void =>{
      setValue(e.currentTarget.value);
    }
    return {
      value,
      onchange: handleChange,
    };
  }

  const router = useRouter();
  //const username = useFormInput('');
  //const password = useFormInput('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] =useState<boolean>(false);
  const [error, setError] =useState<string>('');

  const onChangeUsername = (e: React.FormEvent<HTMLInputElement>): void => {
    setUsername( e.currentTarget.value );
  };
  const onChangePassword = (e: React.FormEvent<HTMLInputElement>): void => {
    setPassword( e.currentTarget.value );
  };

  const handleLogin = async() => {
    setLoading(true);
    const settings = {
      mode: 'no-cors',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({username: username, password: password}),
    };
    try{
      const login = await fetch('http://relengapiv1.relengcorp.com/api/login', settings);
      const respuesta = await login.json();
      router.push("/main");
    }
    catch (e){
      router.push("/main");
      setLoading(false);
    }
  };
  
  return (
    <div>
      <Head>
        <title>Login</title>
        <meta name="description" content="Login de la aplicacion" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="flex flex-row w-full h-screen bg-center bg-cover bg-fondo-login">
          <div className="flex-1 hidden md:block">
          </div>
          <div className="relative flex flex-col items-center justify-center flex-1 h-full bg-bluelogin bg-opacity-80">
            <div className="absolute top-4 right-4 h-14 w-52">
              <Image src="/img/logo.png" alt="Logo" layout="fill" objectFit="cover" />
            </div>
            <div className="flex items-center w-5/6 sm:w-1/2">
              <form className="flex flex-col min-w-full space-y-4">
                <div className="my-10">
                  <p className="text-white">
                    Manual Interactivo
                  </p> 
                  <p className="font-sans text-3xl antialiased font-bold text-white ">
                    BIENVENIDOS DE VUELTA
                  </p> 
                </div> 
                <div>
                  <div className="flex flex-row">
                    <UserIcon className="w-5 h-5 mr-2 text-yellow-500" />
                    <p className="text-white">
                      Usuario
                    </p> 
                  </div>
                  <input 
                    className="min-w-full px-3 py-2 placeholder-white bg-gray-300 rounded-lg bg-opacity-50" 
                    id="username" 
                    type="text" 
                    placeholder="Ingresa tu usuario"
                    value={username}
                    onChange={onChangeUsername}
                  >
                  </input>
                </div>
                <div>
                  <div className="flex flex-row">
                    <LockClosedIcon className="w-5 h-5 mr-2 text-yellow-500" />
                    <p className="text-white">
                      Contraseña
                    </p> 
                  </div>
                  <input 
                    className="min-w-full px-3 py-2 placeholder-white bg-gray-300 rounded-lg bg-opacity-50" 
                    id="username" 
                    type="password" 
                    placeholder="Ingresa tu contraseña"
                    value={password}
                    onChange={onChangePassword}
                  >
                  </input>
                </div>
                <div>
                  <input className="w-5 h-5 align-middle bg-gray-100 border-transparent rounded-sm opacity-50 appearance-none" type="checkbox" />
                  <span className="ml-2 text-sm text-white">
                    Recuerdame
                  </span>
                </div>
                <div>
                  <button 
                    className="min-w-full px-3 py-2 text-center text-white bg-blueloginbutton rounded-md"
                    value={loading ? 'Cargando':'Iniciar Sesion'}
                    onClick={handleLogin}
                    disabled={loading}
                  >
                    Iniciar Sesión
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

(Login as PageWithLayoutType).layout = MainLayout;

export default Login;
